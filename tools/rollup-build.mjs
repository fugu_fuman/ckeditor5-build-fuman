import {rollup} from 'rollup';
import nodeResolve from '@rollup/plugin-node-resolve';
import svg from 'rollup-plugin-svg'
import css from 'rollup-plugin-css-only'
// import css from 'rollup-plugin-css'
import fs from "fs";

function sanitizePackageName(filepath) {
	const dirs = filepath.split('/');
	const file = dirs.pop();
	return [...dirs.map((path) => path.replace(/\.js$/i, 'js')), file].join('/');
}

function getWebDependencyName(dep) {
	return dep.replace(/\.m?js$/i, '');
}

let promise = Promise.resolve();
const pkg = JSON.parse(fs.readFileSync('../package.json'));
const inputs = {};
const sources = new Map();

inputs['ckeditor'] = '../src/ckeditor.js'

// for(const [dep, version] of Object.entries(pkg.dependencies)){
// 	const key = dep.replace('@', '');
//     const packageDir = `../node_modules/${dep}/`;
//     const resultName = dep.includes('ckeditor5')
//                        ? dep.split('-').slice(1).join('-') + '.js'
//                        : dep + '.js';
//
//     // const output = {file: '../dist/' + resultName, format:'esm'};
//
//     if(fs.existsSync(packageDir + 'rollup.config.js')){
//         const {default:packageConfig} = await import(packageDir + 'rollup.config.js');
// 		inputs[key] = packageDir + packageConfig.input
// 		sources.set(key, packageConfig.input);
//     }
//     else {
//         const depPgk = JSON.parse(fs.readFileSync(packageDir + 'package.json'));
//         const file = (depPgk.module ?? depPgk.main);
//
//         if(file.includes('src/')){
// 			inputs[key] = packageDir + file;
// 			sources.set(key, file);
// 		}
//         else {
// 			// fs.copyFileSync(packageDir + file, '../dist/' + resultName);
// 		}
//     }
// }


function replaceBasics(code){
	return code.replace('src/', '').replace('node_modules/', 'lib/').replace('@ckeditor/', 'ckeditor5/').replace('ckeditor5-', '');
}

promise = promise.then(() => rollup({
	input:     Object.values(inputs),
	treeshake: false,
	// preserveEntrySignatures: false,
	plugins: [
		{
			name: 'test',
			async transform(code, id) {
				return {moduleSideEffects: false};
			},

			// async moduleParsed(info) {
			// 	// info.hasModuleSideEffects = false;
			// 	info.id = info.id.replace('src', '').replace('ckeditor5-', '').replace('@ckeditor', '').replace(/'[\\\/]{2,}'/, '');
			// },

			renderChunk(code, chunk, options) {
				const normalizedFileName = chunk.fileName.replaceAll('\\','/');
				const wasInSrc = normalizedFileName.includes('src/');
				const pathParts = normalizedFileName.split('/');
				const startWith = wasInSrc ? '../'.repeat(pathParts.length - (pathParts.indexOf('src')+1)) : false;

				chunk.fileName = replaceBasics(normalizedFileName);

				return code.replace(/import .*\n/g, function (contents) {
					if(!contents.includes(' from ')){
						return '';
					}

					if(wasInSrc && contents.includes('\'' + startWith)){
						contents = contents.replace('../', '');
					}

					return replaceBasics(contents).replace(/'(\w*)\//, '\'./$1/');
				});
			}

			// generateBundle(outputOptions, outputBundle, isWrite){
			// 	for(const [id, chunk] of Object.entries(outputBundle)){
			// 		chunk.fileName = chunk.fileName.replace(/src[\\/]/i, '').replace('ckeditor5-', '');
			// 	}
			// }
		},
		svg(),
		css({output: 'main.css'}),
		nodeResolve()
		// multiInput.default({
		// 	relative: 'src/',
		// 	transformOutputPath: (output, input) => `src/${path.basename(output)}`,
		// })
	]
}).then(bundle => bundle.write({
	format:              'esm',
	dir:                 '../dist',
	exports:             'auto',
	preserveModules:     true,
	preserveModulesRoot: 'src'
})));

promise.catch(err => console.error(err.stack)); // eslint-disable-line no-console
await promise;

// const input2 = {};
//
// fs.readdirSync('../dist/@ckeditor').forEach(function (file, index) {
// 	const sourcePath = path.join('../dist/@ckeditor', file, 'src', 'index.js');
// 	if(fs.existsSync(sourcePath)){
// 		input2[file.replace('ckeditor5-', '')] = sourcePath;
// 	}
// 	else {
// 		console.log('file not found', sourcePath);
// 	}
//
// 	// Make one pass and make the file complete
// 	// if(sources.has('ckeditor/' + file)){
// 	// 	input2.push(path.join('../dist/@ckeditor', file, sources.get('ckeditor/' + file)))
// 	// }
// 	// else {
// 	// 	console.log('could not found', file);
// 	// }
// });
//
// promise = promise.then(() => rollup({
// 	input: input2,
// 	treeshake: {moduleSideEffects:false},
// 	plugins: [
// 		svg(),
// 		css({output:'main.css'}),
// 		nodeResolve(),
// 		// multiInput.default({
// 		// 	relative: 'src/',
// 		// 	transformOutputPath: (output, input) => `src/${path.basename(output)}`,
// 		// })
// 	]
// }).then(bundle => bundle.write({
// 	format:'esm',
// 	dir:'../dist2',
// 	exports: 'named',
// 	entryFileNames: (chunk) => {
// 		const targetName = getWebDependencyName(chunk.name);
// 		const proxiedName = sanitizePackageName(targetName);
// 		return `${proxiedName}.js`;
// 	},
// 	chunkFileNames: 'common/[name]-[hash].js',
// 	// preserveModules: true,
// 	// preserveModulesRoot: 'src'
// })));


await promise;

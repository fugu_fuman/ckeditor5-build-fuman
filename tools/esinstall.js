import {install} from 'esinstall';
import svg from 'rollup-plugin-svg';
import css from 'rollup-plugin-css-only';
import fs from "fs";
import path from "path";

const pkg = JSON.parse(fs.readFileSync('../package.json'));

await install(Object.keys(pkg.dependencies), {
	dest: '../dist-esm',
	sourcemap: false,
	preventAssignment: true,
	treeshake: false,
	rollup: {plugins: [svg(), css({output:'main.css'})]}
});

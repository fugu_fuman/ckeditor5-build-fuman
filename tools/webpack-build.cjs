const { bundler, styles } = require( '@ckeditor/ckeditor5-dev-utils' );
const CKEditorWebpackPlugin = require( '@ckeditor/ckeditor5-dev-webpack-plugin' );
const TerserPlugin = require( 'terser-webpack-plugin' );
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const webpack = require('webpack'); //to access webpack runtime
const fs = require('fs');
const path = require('path');
const entry = {};

fs.readdirSync('../dist/@ckeditor').forEach(function (file, index) {
	const sourcePath = path.join('../dist/@ckeditor', file, 'src', 'index.js');

	if(fs.existsSync(sourcePath)){
		entry[file] = sourcePath;
	}
	else {
		console.log('file not found', sourcePath);
	}
});


const configuration = {
	// devtool: 'source-map',
	performance: { hints: false },
	experiments: {
		outputModule: true
	},

	entry: entry,

	output: {
		// The name under which the editor will be exported.
		// library: 'ClassicEditor',
		filename: '[name].js',
		path: path.resolve( __dirname, '../build2' ),
		libraryTarget: 'module',
		libraryExport: 'default'
	},

	optimization: {
		// minimizer: [
		// 	new TerserPlugin( {
		// 		// sourceMap: true,
		// 		terserOptions: {
		// 			output: {
		// 				// Preserve CKEditor 5 license comments.
		// 				comments: /^!/
		// 			}
		// 		},
		// 		extractComments: false
		// 	} )
		// ]
	},

	plugins: [
		// new CKEditorWebpackPlugin( {
		// 	// UI language. Language codes follow the https://en.wikipedia.org/wiki/ISO_639-1 format.
		// 	// When changing the built-in language, remember to also change it in the editor's configuration (src/ckeditor.js).
		// 	language: 'en',
		// 	additionalLanguages: 'all'
		// } ),
		// new webpack.BannerPlugin( {
		// 	banner: bundler.getLicenseBanner(),
		// 	raw: true
		// } ),
		new MiniCssExtractPlugin()
	],

	module: {
		rules: [
			{
				test: /\.svg$/,
				use: 'raw-loader'
			},
			{
				test: /\.css$/i,
				use: [MiniCssExtractPlugin.loader, 'css-loader'],
			}
		]
	}
}

let compiler = webpack(configuration);
new webpack.ProgressPlugin().apply(compiler);
compiler.run(function (err, stats) {

});

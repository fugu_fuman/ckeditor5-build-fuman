/**
 * @license Copyright (c) 2003-2021, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or https://ckeditor.com/legal/ckeditor-oss-license
 */

'use strict';

/* eslint-env node */
const path = require( 'path' );
const webpack = require( 'webpack' );
const { bundler, styles } = require( '@ckeditor/ckeditor5-dev-utils' );
const CKEditorWebpackPlugin = require( '@ckeditor/ckeditor5-dev-webpack-plugin' );
const TerserPlugin = require( 'terser-webpack-plugin' );
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const buildPath = path.resolve( __dirname, 'build' );

module.exports = {
	devtool: 'source-map',
	performance: { hints: false },

	devServer: {
		publicPath: '/assets/',
		contentBase: [
			path.resolve( __dirname, 'sample' ),
			path.resolve( __dirname, 'tests' ),
		],
		watchContentBase: true,
		compress: true
	},

	experiments: {
		outputModule: true
	},

	entry: path.resolve( __dirname, 'src', 'fumaneditor.js' ),
	
	output: {
		path: buildPath,
		filename: 'ckeditor.js',
		libraryTarget: 'module',
	},

	optimization: {
		usedExports: true,
		
		minimizer: [
			new TerserPlugin( {
				// sourceMap: true,
				terserOptions: {
					output: {
						// Preserve CKEditor 5 license comments.
						comments: /^!/
					}
				},
				extractComments: false
			} )
		]
	},

	plugins: [
		new CKEditorWebpackPlugin( {
			// UI language. Language codes follow the https://en.wikipedia.org/wiki/ISO_639-1 format.
			// When changing the built-in language, remember to also change it in the editor's configuration (src/ckeditor.js).
			language: 'en',
			additionalLanguages: 'all'
		} ),
		new webpack.BannerPlugin( {
			banner: bundler.getLicenseBanner(),
			raw: true
		} ),
		new MiniCssExtractPlugin()
	],

	module: {
		rules: [
			{
				test: /\.svg$/,
				use: 'raw-loader'
			},
			{
				test: /ckeditor5-[^/\\]+[/\\]theme[/\\].+\.css$/,
				use: [
					MiniCssExtractPlugin.loader,
					'css-loader',
					{
						loader: 'postcss-loader',
						options: {
							postcssOptions: styles.getPostCssConfig( {
								themeImporter: {
									themePath: require.resolve( '@ckeditor/ckeditor5-theme-lark' )
								},
								minify: true
							} )
						}
					}
				]
			}
		]
	}
};

import {EmitterMixin} from '@ckeditor/ckeditor5-utils';
import mix from '@ckeditor/ckeditor5-utils/src/mix.js';
import View from '@ckeditor/ckeditor5-ui/src/view.js';

export class CommandView {
    /** @type {View} */
    view;

    /** @type {FumanEditor} */
    editor;

    constructor(editor, view) {
        this.editor = editor;
        this.view = view;
    }
    
    attach(){}

    detach(){}
}

mix( CommandView, EmitterMixin );

export class FormatCommandView extends CommandView {
    /** @type {Map<String, string>} */
    commands;

    currentValue;

    constructor(editor, element) {
        super(editor, element);
        this.commands = new Map();
    }
    
    addCommand(value, descriptor){
        this.commands.set(value, descriptor.modelName);
    }
    
    attach(){
        this.view.on('execute', this.execute);
    }
    
    detach(){
        this.view.off('execute', this.execute);
    }

    execute = (event) => {
        const value = event.source.id;
        if(this.commands.has(value)){
            const command = this.commands.get(value);
            this.editor.execute(...(Array.isArray(command) ? command : [command] ));
            this.editor.editing.view.focus();
        }
    }
}

export class BlockCommandView extends FormatCommandView {
    addCommand(value, descriptor){
        this.commands.set(value, descriptor.isList ? descriptor.modelName : ['paragraph', {value: descriptor.modelName}] );
    }
}

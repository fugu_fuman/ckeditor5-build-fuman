import InputView from '@ckeditor/ckeditor5-ui/src/input/inputview.js';

export default class Checkboxview extends InputView {
    /**
     * @inheritDoc
     */
    constructor( locale ) {
        super(locale);

        const bind = this.bindTemplate;

        this.setTemplate( {
            tag: 'input',
            attributes: {
                type: 'checkbox',
                class: [
                    'ck',
                    'ck-input',
                    bind.if( 'isFocused', 'ck-input_focused' ),
                    bind.if( 'isEmpty', 'ck-input-text_empty' ),
                    bind.if( 'hasError', 'ck-error' )
                ],
                id: bind.to( 'id' ),
                readonly: bind.to( 'isReadOnly' ),
                'aria-invalid': bind.if( 'hasError', true ),
                'aria-describedby': bind.to( 'ariaDescribedById' ),
                style: 'width:2.2em;height:2.2em;border:1px solid #b1b1b1;border-radius:2px;margin:0 5px;min-width:0;',
            },
            on: {
                change: bind.to( this._updateIsEmpty.bind( this ) )
            }
        } );
    }
}
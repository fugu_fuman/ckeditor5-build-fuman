import LabeledFieldView from '@ckeditor/ckeditor5-ui/src/labeledfield/labeledfieldview.js';
import ButtonView from '@ckeditor/ckeditor5-ui/src/button/buttonview.js';
import uid from '@ckeditor/ckeditor5-utils/src/uid.js';
import Checkboxview from './checkboxview.js';
import cancelIcon from '@ckeditor/ckeditor5-core/theme/icons/cancel.svg';
import checkIcon from '@ckeditor/ckeditor5-core/theme/icons/check.svg';

export default class Linkview extends LabeledFieldView {

    constructor( locale, viewCreator ) {
        super(locale, viewCreator);

        const checkboxUid = `ck-checkbox-${ uid() }`;

        this.checkboxView = this.addTargetBlankCheckbox(checkboxUid);
        this.deleteButton = this.addDeleteButton();
        this.addButton = this.addAddButton();

        const bind = this.bindTemplate;

        this.setTemplate( {
            tag: 'div',
            attributes: {
                class: [
                    'ck',
                    'ck-labeled-field-view',
                    bind.to( 'class' ),
                    bind.if( 'isEnabled', 'ck-disabled', value => !value ),
                    bind.if( 'isEmpty', 'ck-labeled-field-view_empty' ),
                    bind.if( 'isFocused', 'ck-labeled-field-view_focused' ),
                    bind.if( 'placeholder', 'ck-labeled-field-view_placeholder' ),
                    bind.if( 'errorText', 'ck-error' )
                ]
            },
            children: [
                {
                    tag: 'div',
                    attributes: {
                        class: [
                            'ck',
                            'ck-labeled-field-view__input-wrapper'
                        ]
                    },
                    children: [
                        this.fieldView,
                        this.labelView,
                        this.checkboxView,
                        this.addButton,
                        this.deleteButton
                    ]
                },
                this.statusView
            ]
        } );
    }

    /**
     * @param {string} checkboxId
     * @returns {Checkboxview}
     */
    addTargetBlankCheckbox(checkboxId) {
        const checkboxView = new Checkboxview( this.locale);

        checkboxView.id = checkboxId;
        checkboxView.bind( 'value' ).to( this );
        checkboxView.bind( 'isReadOnly' ).to( this );
        checkboxView.bind( 'hasError' ).to( this, 'errorText', value => !!value );

        return checkboxView;
    }

    /**
     * @returns {ButtonView}
     */
    addDeleteButton() {
        const deleteButton = new ButtonView(this.locale);
        deleteButton.set({
            label: 'delete',
            icon: cancelIcon,
            tabindex:0
        });

        return deleteButton;
    }

    /**
     * @returns {ButtonView}
     */
    addAddButton() {
        const addButton = new ButtonView(this.locale);
        addButton.set({
            label: 'add',
            icon: checkIcon,
            tabindex:0
        });

        return addButton;
    }
}
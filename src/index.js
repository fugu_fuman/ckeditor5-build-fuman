import FumanEditor from './fumaneditor.js';

FumanEditor.create(document.querySelector( '#editor' ) , {
    plugins: []
    , toolbar: []
    , fuman: {
        "table":"page_blog_post_section"
        ,"inputtype":"text"
        ,"formname":"form_page_blog_post_section_text"
        ,"elementHandlerClassName":"fuman.app.core.inputtype.Textarea"
        ,"showeditor":true
        ,"language":null
        ,"add_uri":true
        ,"add_email":true
        ,"target_blank":false
        ,"uri_label": "Link"
        ,"character_label": "Character"
        ,"block_label": "Block"
        ,"lineBreakType":0
        ,"characters":["noformat","strong.hightlight","em.quote"]
        ,"paragraphs":["p", "ul.list", "ul.test-ul", "ol", "ol.test-ol", "p.promilink", "h2", "h3.test"]
        ,"translations":[
            {"key":"noformat","value":"delete formatting"},
            {"key":"strong.hightlight","value":"Bold"},
            {"key":"em.quote","value":"Italic"},
            {"key":"p","value":"Paragraph"},
            {"key":"ul.list","value":"Unordered List"},
            {"key":"ul.test-ul","value":"Unordered test-ul"},
            {"key":"ol","value":"Ordered List"},
            {"key":"ol.test-ol","value":"Ordered test-ol"},
            {"key":"p.promilink","value":"Promilink"},
            {"key":"h2","value":"header"},
            {"key":"h3.test","value":"sub-header"}
        ]
    }
}).then(editor=> {
    document.getElementById('store').addEventListener('click', function(){
        document.getElementById('storage').value = editor.getData();
    });
    document.getElementById('back').addEventListener('click', function(){
        editor.setData(document.getElementById('storage').value);
    });
});


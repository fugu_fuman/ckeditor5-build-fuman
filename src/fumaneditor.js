import BalloonEditor from '@ckeditor/ckeditor5-editor-balloon/src/ballooneditor.js';
import {isElement} from 'lodash-es';
import CKEditorError from '@ckeditor/ckeditor5-utils/src/ckeditorerror.js';
import getDataFromElement from '@ckeditor/ckeditor5-utils/src/dom/getdatafromelement.js';
import mix from '@ckeditor/ckeditor5-utils/src/mix.js';
import DataApiMixin from '@ckeditor/ckeditor5-core/src/editor/utils/dataapimixin.js';
import ElementApiMixin from '@ckeditor/ckeditor5-core/src/editor/utils/elementapimixin.js';
import {NoFormatCommand} from './command/noformat.js';
import FormatCommand from './command/format.js';
import {Collection, Descriptor} from './schema.js';
import Format from './plugin/format.js';
import Link from './plugin/link.js';
import Block from './plugin/block.js';
import FumanEssentials from './plugin/essentials.js';
import Indent from '@ckeditor/ckeditor5-indent/src/indent.js';
import PasteFromOffice from '@ckeditor/ckeditor5-paste-from-office/src/pastefromoffice.js';
import TextTransformation from '@ckeditor/ckeditor5-typing/src/texttransformation.js';
import Hyphens from './plugin/hyphens.js';

const upcastTypeGroups = [
	['strong', 'b'], ['em', 'i']
];

const paragraphLikeElements = new Set( [
	'blockquote',
	'dd',
	'div',
	'dt',
	'h1',
	'h2',
	'h3',
	'h4',
	'h5',
	'h6',
	'li',
	'p',
	'td',
	'th'
] );

/**
 * Converts the remaining paragraph-like elements to the provided (paragraph) modelName
 * @param {FumanEditor} editor
 * @param {String} modelName
 */
function autoUpCast(editor, modelName){
	// Conversion for paragraph-like elements which has not been converted by any plugin.
	editor.conversion.for( 'upcast' ).elementToElement( {
		model: ( viewElement, { writer } ) => {
			if ( !paragraphLikeElements.has( viewElement.name ) ) {
				return null;
			}

			// Do not auto-paragraph empty elements.
			if ( viewElement.isEmpty ) {
				return null;
			}

			return writer.createElement( modelName );
		},
		view: /.+/,
		converterPriority: 'low'
	} );
}

/**
 * Register a listener to the enter command, which renames a heading element to the defaultModelName (paragraph) of the editor when it is after a heading element
 * -> prevents creating multiple heading element directly one after the other.
 * @param {FumanEditor} editor
 * @param {String} defaultModelName
 * @param {Array<Descriptor>} headings
 */
function ensureNonHeadingBlockAfterHeading(editor, defaultModelName, headings){
	const enterCommand = editor.commands.get( 'enter' );
	
	if ( enterCommand ) {
		editor.listenTo( enterCommand, 'afterExecute', ( evt, data ) => {
			const positionParent = editor.model.document.selection.getFirstPosition().parent;
			const isHeading = headings.some( descriptor => positionParent.is( 'element', descriptor.modelName ) );
			if ( isHeading && !positionParent.is( 'element', defaultModelName ) && positionParent.childCount === 0 ) {
				data.writer.rename( positionParent, defaultModelName );
			}
		} );
	}
}

/**
 * @mixes {ElementApiMixin}
 * @mixes {DataApiMixin}
 */
export default class FumanEditor extends BalloonEditor {
	/**
	 * Creates an instance of the classic editor.
	 *
	 * **Note:** do not use the constructor to create editor instances. Use the static
	 * {@link module:editor-classic/classiceditor~ClassicEditor.create `ClassicEditor.create()`} method instead.
	 *
	 * @protected
	 * @param {HTMLElement|String} sourceElementOrData The DOM element that will be the source for the created editor
	 * or the editor's initial data. For more information see
	 * {@link module:editor-classic/classiceditor~ClassicEditor.create `ClassicEditor.create()`}.
	 * @param {module:core/editor/editorconfig~EditorConfig} config The editor configuration.
	 */
	constructor( sourceElementOrData, config ) {
		super( sourceElementOrData, config);
		
		this.createSchema();
	}

	createSchema(){
		const options = this.config.get( 'fuman' );

		if('characters' in options && options.characters.length>0){
			const noFormatCommand = new NoFormatCommand( this );
			for(const tagAndClassName of options.characters){
				const descriptor = this.createDescriptorFrom(tagAndClassName);

				if(tagAndClassName === 'noformat'){
					this.commands.add( descriptor.modelName, noFormatCommand);
				}
				else {
					noFormatCommand.attributes.push(descriptor.modelName);

					this.model.schema.extend( '$text', { allowAttributes: descriptor.modelName } );
					this.model.schema.setAttributeProperties( descriptor.modelName, {
						isFormatting: true,
						copyOnEnter: true
					} );
					this.commands.add(descriptor.modelName, new FormatCommand( this, descriptor.modelName ) );

					const upcastAlso = [];
					upcastTypeGroups.forEach(tagGroup=>{
						if(tagGroup.includes(descriptor.name)){
							upcastAlso.push(...tagGroup.filter(tag=>tag!==descriptor.name));
						}
					});

					if(descriptor.classes){
						upcastAlso.push({classes: descriptor.classes});
					}

					this.conversion.attributeToElement( {
						model: descriptor.modelName,
						view: descriptor.classes ? {name:descriptor.name, classes:descriptor.classes} : descriptor.name,
						upcastAlso: upcastAlso
					});
				}
			}

			this.config._config.plugins.push(Format);
		}

		if('paragraphs' in options && options.paragraphs.length>0) {
			let defaultDescriptor = null;
			const headings = [], schemaBuilderCollection = new Collection();

			for (const tagAndClassName of options.paragraphs) {
				const descriptor = this.createDescriptorFrom(tagAndClassName);

				if (descriptor.name === 'paragraph') {
					descriptor.name = 'p';
				}
				
				if (descriptor.name.substring(0, 1) === 'h') {
					headings.push(descriptor);
				}

				if (defaultDescriptor === null || (descriptor.name === 'p' && defaultDescriptor.name !== 'p')) {
					defaultDescriptor = descriptor;
				}

				schemaBuilderCollection.createAndAddByDescriptor(descriptor);
			}
			
			if(defaultDescriptor){
				autoUpCast(this, defaultDescriptor.modelName);
				if (defaultDescriptor.name.split(0, 1) !== 'h' && headings.length > 0) {
					this.on('ready', ()=>ensureNonHeadingBlockAfterHeading(this, defaultDescriptor.modelName, headings));
				}

				//Link/Alias the defaultDescriptor to paragraph model, this is used everywhere in ckeditor
				this.model.schema.register('paragraph', {
					inheritAllFrom: '$block'
				});

				this.conversion.elementToElement({
					model: 'paragraph',
					view:  defaultDescriptor.classes ? {name: defaultDescriptor.name, classes: defaultDescriptor.classes} : defaultDescriptor.name,
				});
			}
			
			schemaBuilderCollection.builder.forEach(builder=>builder.apply(this));
		}
	}

	createDescriptorFrom(tagAndClassName){
		return Descriptor.createFromTagAndClassName(tagAndClassName);
	}

	/**
	 * Destroys the editor instance, releasing all resources used by it.
	 *
	 * Updates the editor's source element with the data.
	 *
	 * @returns {Promise}
	 */
	destroy() {
		// Cache the data, then destroy.
		// It's safe to assume that the model->view conversion will not work after super.destroy().
		const data = this.getData();

		this.ui.destroy();

		return super.destroy()
			.then( () => {
				if ( this.sourceElement ) {
					setDataInElement( this.sourceElement, data );
				}
			} );
	}

	/**
	 * Creates a new balloon editor instance.
	 *
	 * There are three general ways how the editor can be initialized.
	 *
	 * # Using an existing DOM element (and loading data from it)
	 *
	 * You can initialize the editor using an existing DOM element:
	 *
	 *		BalloonEditor
	 *			.create( document.querySelector( '#editor' ) )
	 *			.then( editor => {
	 *				console.log( 'Editor was initialized', editor );
	 *			} )
	 *			.catch( err => {
	 *				console.error( err.stack );
	 *			} );
	 *
	 * The element's content will be used as the editor data and the element will become the editable element.
	 *
	 * # Creating a detached editor
	 *
	 * Alternatively, you can initialize the editor by passing the initial data directly as a string.
	 * In this case, the editor will render an element that must be inserted into the DOM for the editor to work properly:
	 *
	 *		BalloonEditor
	 *			.create( '<p>Hello world!</p>' )
	 *			.then( editor => {
	 *				console.log( 'Editor was initialized', editor );
	 *
	 *				// Initial data was provided so the editor UI element needs to be added manually to the DOM.
	 *				document.body.appendChild( editor.ui.element );
	 *			} )
	 *			.catch( err => {
	 *				console.error( err.stack );
	 *			} );
	 *
	 * This lets you dynamically append the editor to your web page whenever it is convenient for you. You may use this method if your
	 * web page content is generated on the client side and the DOM structure is not ready at the moment when you initialize the editor.
	 *
	 * # Using an existing DOM element (and data provided in `config.initialData`)
	 *
	 * You can also mix these two ways by providing a DOM element to be used and passing the initial data through the configuration:
	 *
	 *		BalloonEditor
	 *			.create( document.querySelector( '#editor' ), {
	 *				initialData: '<h2>Initial data</h2><p>Foo bar.</p>'
	 *			} )
	 *			.then( editor => {
	 *				console.log( 'Editor was initialized', editor );
	 *			} )
	 *			.catch( err => {
	 *				console.error( err.stack );
	 *			} );
	 *
	 * This method can be used to initialize the editor on an existing element with the specified content in case if your integration
	 * makes it difficult to set the content of the source element.
	 *
	 * Note that an error will be thrown if you pass the initial data both as the first parameter and also in the configuration.
	 *
	 * # Configuring the editor
	 *
	 * See the {@link module:core/editor/editorconfig~EditorConfig editor configuration documentation} to learn more about
	 * customizing plugins, toolbar and more.
	 *
	 * # Using the editor from source
	 *
	 * The code samples listed in the previous sections of this documentation assume that you are using an
	 * {@glink builds/guides/overview editor build} (for example – `@ckeditor/ckeditor5-build-balloon`).
	 *
	 * If you want to use the balloon editor from source (`@ckeditor/ckeditor5-editor-balloon/src/ballooneditor`),
	 * you need to define the list of
	 * {@link module:core/editor/editorconfig~EditorConfig#plugins plugins to be initialized} and
	 * {@link module:core/editor/editorconfig~EditorConfig#toolbar toolbar items}. Read more about using the editor from
	 * source in the {@glink builds/guides/integration/advanced-setup "Advanced setup" guide}.
	 *
	 * @param {HTMLElement|String} sourceElementOrData The DOM element that will be the source for the created editor
	 * or the editor's initial data.
	 *
	 * If a DOM element is passed, its content will be automatically loaded to the editor upon initialization.
	 * Moreover, the editor data will be set back to the original element once the editor is destroyed.
	 *
	 * If the initial data is passed, a detached editor will be created. In this case you need to insert it into the DOM manually.
	 * It is available under the {@link module:editor-balloon/ballooneditorui~BalloonEditorUI#element `editor.ui.element`} property.
	 *
	 * @param {module:core/editor/editorconfig~EditorConfig} [config] The editor configuration.
	 * @returns {Promise} A promise resolved once the editor is ready. The promise resolves with the created editor instance.
	 */
	static create( sourceElementOrData, config = {} ) {
		return new Promise( resolve => {
			const isHTMLElement = isElement( sourceElementOrData );

			if ( isHTMLElement && sourceElementOrData.tagName === 'TEXTAREA' ) {
				// Documented in core/editor/editor.js
				// eslint-disable-next-line ckeditor5-rules/ckeditor-error-message
				throw new CKEditorError( 'editor-wrong-element', null );
			}

			const fumanConfig = config.fuman;
			const plugins = config.plugins || (config.plugins = []);
			plugins.push(...FumanEditor.builtinPlugins);
			
			if(fumanConfig.add_uri || fumanConfig.add_email){
				config.toolbar.push('link');
				config.toolbar.push('|');
			}
			if('characters' in fumanConfig && fumanConfig.characters.length>0){
				config.toolbar.push('format');
				config.toolbar.push('|');
			}
			if(addBlockPlugin(fumanConfig)) {
				config.toolbar.push('block', 'outdent', 'indent');
				plugins.push(Block);
			}

			if(fumanConfig.add_uri || fumanConfig.add_email){
				if(!('link' in config)){
					config.link = {};
				}

				if(!('decorators' in config.link)){
					config.link.decorators = {};
				}

				config.link.decorators.external = {
					mode: 'manual',
					label: 'Open in a new tab',
					defaultValue: fumanConfig.target_blank,
					attributes: {
						target: '_blank',
						rel: 'noopener noreferrer'
					}
				}

				plugins.push(Link);
			}

			const editor = new this( sourceElementOrData, config );

			resolve(
				editor.initPlugins()
					.then( () => editor.ui.init())
					.then( () => {
						if ( !isHTMLElement && config.initialData ) {
							// Documented in core/editor/editorconfig.jdoc.
							// eslint-disable-next-line ckeditor5-rules/ckeditor-error-message
							throw new CKEditorError( 'editor-create-initial-data', null );
						}
						
						const initialData = config.initialData !== undefined ? config.initialData : getInitialData( sourceElementOrData );
						return editor.data.init( initialData );
					} )
					.then( () => editor.fire( 'ready' ) )
					.then( () => editor )
			);
		} );
	}
}

mix( FumanEditor, DataApiMixin );
mix( FumanEditor, ElementApiMixin );

function getInitialData( sourceElementOrData ) {
	return isElement( sourceElementOrData ) ? getDataFromElement( sourceElementOrData ) : sourceElementOrData;
}

function addBlockPlugin(config) {
	let doAdd = 'paragraphs' in config && config.paragraphs.length>0;
	if (doAdd && config.paragraphs.length === 1 && config.paragraphs[0] === 'p') {
		doAdd = false;
	}

	return doAdd;
}

FumanEditor.builtinPlugins = [
	FumanEssentials,
	Indent,
	PasteFromOffice,
	TextTransformation,
	Hyphens
]

// Editor configuration.
FumanEditor.defaultConfig = {
	// This value must be kept in sync with the language defined in webpack.config.js.
	language: 'de',
};

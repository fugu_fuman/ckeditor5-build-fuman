import {
    cleanList, cleanListItem,
    modelChangePostFixer, modelIndentPasteFixer,
    modelToViewPosition,
    modelViewChangeIndent,
    modelViewChangeType, modelViewInsertion,
    modelViewMergeAfter,
    modelViewMergeAfterChangeType,
    modelViewRemove,
    modelViewSplitOnInsert, viewModelConverter,
    viewToModelPosition
} from '@ckeditor/ckeditor5-list/src/list/converters.js';
import IndentCommand from '@ckeditor/ckeditor5-list/src/list/indentcommand.js';
import ListCommand from './command/list.js';
import BlockCommand from './command/block.js';
import {mergeViewLists} from '@ckeditor/ckeditor5-list/src/list/utils.js';
import blockAutoformatEditing from '@ckeditor/ckeditor5-autoformat/src/blockautoformatediting.js';
import EmitterMixin from '@ckeditor/ckeditor5-utils/src/emittermixin.js';

export class Descriptor {
    static createFromTagAndClassName(tagAndClassName){
        let name = '', classes = null, config = null;

        if(tagAndClassName.includes(':')){
            let configs;
            [tagAndClassName, ...configs] = tagAndClassName.split(':');
            config = configs.join(':');
        }

        if(tagAndClassName.includes('.')){
            [name, ...classes] = tagAndClassName.split('.');
        }
        else {
            name = tagAndClassName;
        }
        
        return new this(name, name + '-' + (classes ? classes.join('.') : 'fuman'), classes, config);
    }
    
    name;
    modelName;
    classes;
    isList;
    config;
    
    constructor(name, modelName, classes, config) {
        this.name = name;
        this.modelName = modelName;
        this.classes = classes;
        this.config = config;
        
        this.isList = ['ol', 'ul'].includes(this.name);
    }
}

export class Collection {
    /** @type {Map<Class, BasicBlockBuilder>} */
    builder;

    constructor() {
        this.builder = new Map();
    }

    /**
     * @param {Descriptor} descriptor
     */
    createAndAddByDescriptor(descriptor){
        const Type = descriptor.isList ? ListBuilder : BasicBlockBuilder;
        let builder = this.builder.get(Type);
        
        if(!builder){
            this.builder.set(Type, (builder = new Type()));
        }
        
        builder.addDescriptor(descriptor);
    }
}


export class BasicBlockBuilder {
    /** @type {Descriptor[]} */
    descriptors;
    
    constructor() {
        this.descriptors = [];
    }

    addDescriptor(descriptor) {
        this.descriptors.push(descriptor);
    }

    apply(editor){
        const modelNames = [];
        
        for(const descriptor of this.descriptors){
            editor.model.schema.register(descriptor.modelName, {
                inheritAllFrom: '$block'
            });

            editor.conversion.elementToElement({
                model: descriptor.modelName,
                view:  descriptor.classes ? {name: descriptor.name, classes: descriptor.classes} : descriptor.name,
                converterPriority: descriptor.classes ? 'high' : 'normal' //descriptors with classes are more specific (don't overwrite), therefore need higher priority 
            });

            modelNames.push(descriptor.modelName);
        }

        editor.commands.add('paragraph', new BlockCommand(editor, modelNames));
    }
}


export class ListBuilder extends EmitterMixin(BasicBlockBuilder) {
    apply(editor){
        // Schema.
        // Note: in case `$block` will ever be allowed in `listItem`, keep in mind that this feature
        // uses `Selection#getSelectedBlocks()` without any additional processing to obtain all selected list items.
        // If there are blocks allowed inside list item, algorithms using `getSelectedBlocks()` will have to be modified.
        editor.model.schema.register( 'listItem', {
            inheritAllFrom: '$block',
            allowAttributes: [ 'listType', 'listIndent', 'classes' ]
        } );

        // Converters.
        const data = editor.data;
        const editing = editor.editing;

        editor.model.document.registerPostFixer( writer => modelChangePostFixer( editor.model, writer ) );

        editing.mapper.registerViewToModelLength( 'li', getViewListItemLength );
        data.mapper.registerViewToModelLength( 'li', getViewListItemLength );

        editing.mapper.on( 'modelToViewPosition', modelToViewPosition( editing.view ) );
        editing.mapper.on( 'viewToModelPosition', viewToModelPosition( editor.model ) );
        data.mapper.on( 'modelToViewPosition', modelToViewPosition( editing.view ) );

        //used for generating the html used in the editor
        editor.conversion.for( 'editingDowncast' )
            .add( dispatcher => {
                dispatcher.on( 'insert', modelViewSplitOnInsert, { priority: 'high' } );
                dispatcher.on( 'insert:listItem', modelViewInsertion( editor.model ) );
                dispatcher.on( 'insert:listItem', downcastClasses, { priority: 'low' } );
                dispatcher.on( 'attribute:classes:listItem', modelViewChangeClasses, { priority: 'low' } );
                dispatcher.on( 'attribute:classes:listItem', modelViewMergeAfterChangeType, { priority: 'low' } );//we use the same logic for merging classes as for types
                dispatcher.on( 'attribute:listType:listItem', modelViewChangeType, { priority: 'high' } );
                dispatcher.on( 'attribute:listType:listItem', modelViewMergeAfterChangeType, { priority: 'low' } );
                dispatcher.on( 'attribute:listIndent:listItem', modelViewChangeIndent( editor.model ) );
                dispatcher.on( 'remove:listItem', modelViewRemove( editor.model ) );
                dispatcher.on( 'remove', modelViewMergeAfter, { priority: 'low' } );
            } );

        //used for generating the value (html) of the content managed by the editor -> accessed by getData
        editor.conversion.for( 'dataDowncast' )
            .add( dispatcher => {
                dispatcher.on( 'insert', modelViewSplitOnInsert, { priority: 'high' } );
                dispatcher.on( 'insert:listItem', modelViewInsertion( editor.model ) );
                dispatcher.on( 'insert:listItem', downcastClasses, { priority: 'low' } );
            } );

        //used when value (html) is set to the editor -> parse html
        editor.conversion.for( 'upcast' )
            .add( dispatcher => {
                dispatcher.on( 'element:ul', cleanList, { priority: 'high' } );
                dispatcher.on( 'element:ol', cleanList, { priority: 'high' } );
                dispatcher.on( 'element:li', cleanListItem, { priority: 'high' } );
                dispatcher.on( 'element:li', viewModelConverter );
                dispatcher.on( 'element:li', upcastClasses, { priority: 'low' });
            } );

        // Fix indentation of pasted items.
        editor.model.on( 'insertContent', modelIndentPasteFixer, { priority: 'high' } );

        // Register commands for numbered and bulleted list.
        const autoformats = new Map();
        for(const descriptor of this.descriptors){
            editor.commands.add( descriptor.modelName, new ListCommand( editor, descriptor.name === 'ol' ? 'numbered' : 'bulleted', descriptor.classes, descriptor.modelName ) );
            autoformats.set(descriptor.name, descriptor.modelName);
        }
        
        autoformats.forEach((name, type)=>{
            blockAutoformatEditing( editor, {isEnabled:true}, type === 'ol' ? /^1[.|)]\s$/ : /^[*-]\s$/, name );
        })

        // Register commands for indenting.
        editor.commands.add( 'indentList', new IndentCommand( editor, 'forward' ) );
        editor.commands.add( 'outdentList', new IndentCommand( editor, 'backward' ) );

        const viewDocument = editing.view.document;

        // Overwrite default Enter key behavior.
        // If Enter key is pressed with selection collapsed in empty list item, outdent it instead of breaking it.
        this.listenTo( viewDocument, 'enter', ( evt, data ) => {
            const doc = editor.model.document;
            const positionParent = doc.selection.getLastPosition().parent;

            if ( doc.selection.isCollapsed && positionParent.name === 'listItem' && positionParent.isEmpty ) {
                editor.execute( 'outdentList' );

                data.preventDefault();
                evt.stop();
            }
        }, { context: 'li' } );

        // Overwrite default Backspace key behavior.
        // If Backspace key is pressed with selection collapsed on first position in first list item, outdent it. #83
        this.listenTo( viewDocument, 'delete', ( evt, data ) => {
            // Check conditions from those that require less computations like those immediately available.
            if ( data.direction !== 'backward' ) {
                return;
            }

            const selection = editor.model.document.selection;

            if ( !selection.isCollapsed ) {
                return;
            }

            const firstPosition = selection.getFirstPosition();

            if ( !firstPosition.isAtStart ) {
                return;
            }

            const positionParent = firstPosition.parent;

            if ( positionParent.name !== 'listItem' ) {
                return;
            }

            const previousIsAListItem = positionParent.previousSibling && positionParent.previousSibling.name === 'listItem';

            if ( previousIsAListItem ) {
                return;
            }

            editor.execute( 'outdentList' );
            data.preventDefault();
            evt.stop();
        }, { context: 'li' } );

        const getCommandExecuter = commandName => {
            return ( data, cancel ) => {
                const command = editor.commands.get( commandName );

                if ( command.isEnabled ) {
                    editor.execute( commandName );
                    cancel();
                }
            };
        };

        editor.keystrokes.set( 'Tab', getCommandExecuter( 'indentList' ) );
        editor.keystrokes.set( 'Shift+Tab', getCommandExecuter( 'outdentList' ) );

        editor.on( 'ready', function(){
            const commands = editor.commands;
            const indent = commands.get( 'indent' );
            const outdent = commands.get( 'outdent' );

            if ( indent ) {
                indent.registerChildCommand( commands.get( 'indentList' ) );
            }

            if ( outdent ) {
                outdent.registerChildCommand( commands.get( 'outdentList' ) );
            }
        });
    }
}

function getViewListItemLength( element ) {
    let length = 1;

    for ( const child of element.getChildren() ) {
        if ( child.name === 'ul' || child.name === 'ol' ) {
            for ( const item of child.getChildren() ) {
                length += getViewListItemLength( item );
            }
        }
    }

    return length;
}

function upcastClasses( evt, data, conversionApi ) {
    const viewItem = data.viewItem;
    const modelRange = data.modelRange;

    const modelElement = modelRange && modelRange.start.nodeAfter;

    if ( !modelElement ) {
        return;
    }

    // The upcast conversion picks up classes from the base element and from the <figure> element so it should be extensible.
    const currentAttributeValue = modelElement.getAttribute( 'classes' ) || [];
    currentAttributeValue.push( ...viewItem.parent.getClassNames() );
    conversionApi.writer.setAttribute( 'classes', currentAttributeValue, modelElement );
}

function downcastClasses( evt, data, conversionApi) {
    const modelElement = data.item;
    const viewWriter = conversionApi.writer;
    let viewItem = conversionApi.mapper.toViewElement( modelElement );
    
    if ( !viewItem ) {
        return;
    }
    
    if(viewItem.name === 'li'){
        viewItem = viewItem.parent;
    }
    
    viewWriter.addClass( modelElement.getAttribute( 'classes' ), viewItem );

    // Merge the inserted view list with its possible neighbor lists.
    mergeViewLists( viewWriter, viewItem, viewItem.nextSibling );
    mergeViewLists( viewWriter, viewItem.previousSibling, viewItem );
}

function modelViewChangeClasses( evt, data, conversionApi ) {
    if ( !conversionApi.consumable.consume( data.item, 'attribute:classes' ) ) {
        return;
    }
    
    const modelElement = data.item;
    const viewItem = conversionApi.mapper.toViewElement( data.item );
    const viewWriter = conversionApi.writer;

    // Break the container after and before the list item.
    // This will create a view list with one view list item -- the one that changed type.
    viewWriter.breakContainer( viewWriter.createPositionBefore( viewItem ) );
    viewWriter.breakContainer( viewWriter.createPositionAfter( viewItem ) );

    // Change the class
    const viewList = viewItem.parent;
    viewWriter.removeAttribute( 'class', viewList );
    viewWriter.addClass( modelElement.getAttribute( 'classes' ), viewList );
}
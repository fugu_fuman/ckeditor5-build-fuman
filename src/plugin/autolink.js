import AutoLink from '@ckeditor/ckeditor5-link/src/autolink.js';
import {addLinkProtocolIfApplicable} from '@ckeditor/ckeditor5-link/src/utils.js';

function isLinkAllowedOnRange( range, model ) {
	return model.schema.checkAttributeInSelection( model.createSelection( range ), 'linkHref' );
}

export function isExternal(value){
	return !(value.includes('fuman://') || value.includes('id_page:'));
}

/**
 * Target attribute aware autolink  
 */
export default class FumanAutoLink extends AutoLink {
	/** @type {Function} */
	isExternalCallback;
	
	afterInit() {
		super.afterInit();
		this.isExternalCallback = this.editor.config.get('fuman.isExternalCallback') || isExternal;
	}

	/**
	 * Applies a link on a given range.
	 *
	 * @param {String} url The URL to link.
	 * @param {module:engine/model/range~Range} range The text range to apply the link attribute to.
	 * @private
	 */
	_applyAutoLink( link, range ) {
		const model = this.editor.model;
		const deletePlugin = this.editor.plugins.get( 'Delete' );

		if ( !this.isEnabled || !isLinkAllowedOnRange( range, model ) ) {
			return;
		}

		// Enqueue change to make undo step.
		model.enqueueChange( writer => {
			const defaultProtocol = this.editor.config.get( 'link.defaultProtocol' );
			const parsedUrl = addLinkProtocolIfApplicable( link, defaultProtocol );
			writer.setAttribute( 'linkHref', parsedUrl, range );
			
			if(this.isExternalCallback(parsedUrl)){
				writer.setAttribute('linkExternal', true, range);
			}

			model.enqueueChange( () => {
				deletePlugin.requestUndoOnBackspace();
			});
		} );
	}
} 
import {createDropdown, addListToDropdown} from '@ckeditor/ckeditor5-ui/src/dropdown/utils.js';
import DropdownView from '@ckeditor/ckeditor5-ui/src/dropdown/dropdownview.js';
import Collection from '@ckeditor/ckeditor5-utils/src/collection.js';
import {BlockCommandView} from '../view.js';
import Model from '@ckeditor/ckeditor5-ui/src/model.js';
import Plugin from '@ckeditor/ckeditor5-core/src/plugin.js';


export default class Block extends Plugin {

    static get pluginName(){
        return 'block';
    }

    /** @type {Object} */
    titles = {};

    /** @type {string} */
    defaultTitle;

    /** @type {DropdownView} */
    dropdownView;
    
    items;

    init(){
        const options = this.editor.config.get( 'fuman' );
        this.defaultTitle = options.block_label
        this.items = options.paragraphs;
        this.editor.ui.componentFactory.add( Block.pluginName, this.buildComponent);
    }
    
    buildComponent = (locale) => {
        const options = this.editor.config.get( 'fuman' );
        this.dropdownView = createDropdown(locale);

        this.dropdownView.buttonView.set({
            label: this.defaultTitle,
            withText: true,
            tabindex: 0,
        });

        const view = this.createCommandView();
        const items = new Collection();
        const commands = new Set();

        for(const tagAndClassName of this.items){
            const descriptor = this.editor.createDescriptorFrom(tagAndClassName);
            view.addCommand(tagAndClassName, descriptor);

            let label = options.translations.filter(item => item.key === tagAndClassName);
            if (label.length > 0) {
                label = label[0].value;
            } else {
                label = tagAndClassName;
            }

            items.add({
                type: 'button',
                model: new Model({
                    id: tagAndClassName,
                    withText: true,
                    label: label,
                })
            });

            this.titles[descriptor.modelName] = label;
            commands.add(this.getCommand(descriptor));
        }
        
        for(const command of commands){
            command.on('change:value', this.updateDropdown);
            command.on('execute', this.updateDropdown);
        }

        addListToDropdown(this.dropdownView, items);
        view.attach();

        return this.dropdownView;
    }
    
    createCommandView(){
        return new BlockCommandView(this.editor, this.dropdownView);
    }

    /**
     * @param {Descriptor} descriptor
     */
    getCommand(descriptor){
        let command = this.editor.commands.get( descriptor.modelName);
        if (!command) {
            command = this.editor.commands.get( 'paragraph');
        }
        return command;
    }

    updateDropdown = (event, property, value, state) => {
        const buttonView = this.dropdownView.buttonView;
        
        // {event.source.value} is true when attribute is detected, false when removed/not detected
        if(event.source.value){
            buttonView.label = this.titles[event.source.commandName ? event.source.commandName : event.source.value]
        }
        else if(!event.source.commandName || buttonView.label === this.titles[event.source.commandName]){
            buttonView.label = this.defaultTitle;
        }
    }
}
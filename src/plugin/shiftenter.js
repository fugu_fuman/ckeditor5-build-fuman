import Plugin from '@ckeditor/ckeditor5-core/src/plugin.js';
import ShiftEnterCommand from '@ckeditor/ckeditor5-enter/src/shiftentercommand.js';
import EnterObserver from '@ckeditor/ckeditor5-enter/src/enterobserver.js';

/**
 * @license Copyright (c) 2003-2021, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or https://ckeditor.com/legal/ckeditor-oss-license
 */

/**
 * This plugin handles the <kbd>Shift</kbd>+<kbd>Enter</kbd> keystroke (soft line break) in the editor.
 *
 * See also the {@link module:enter/enter~Enter} plugin.
 *
 * For more information about this feature see the {@glink api/enter package page}.
 *
 * @extends module:core/plugin~Plugin
 */
class ShiftEnter extends Plugin {
	/**
	 * @inheritDoc
	 */
	static get pluginName() {
		return 'ShiftEnter';
	}

	init() {
		const editor = this.editor;
		const schema = editor.model.schema;
		const conversion = editor.conversion;
		const view = editor.editing.view;
		const viewDocument = view.document;

		// Configure the schema.
		schema.register( 'softBreak', {
			allowWhere: '$text',
			isInline: true
		});

		conversion.for( 'upcast' )
			.elementToElement({
				model: 'softBreak',
				view: 'br',
			})
			.elementToElement({
				model: 'softBreak',
				view: {name: 'span', classes: 'softbreak'},
				converterPriority: 'high',
			});

		conversion.for( 'downcast' )
			.elementToElement({
				model: 'softBreak',
				view: ( modelElement, { writer } ) => writer.createEmptyElement( 'span', {class: 'softbreak'} )
			});
		
		// conversion.elementToElement({
		// 	model: 'softBreak',
		// 	// view:  {name:  'span', classes: 'softbreak'},
		// 	view: ( modelElement, { writer } ) => writer.createEmptyElement( 'span', {class: 'softbreak'} ),
		// 	converterPriority: 'high',
		// 	upcastAlso: ['br']
		//
		// })
		
		view.addObserver( EnterObserver );
		editor.commands.add( 'shiftEnter', new ShiftEnterCommand( editor ) );

		this.listenTo( viewDocument, 'enter', ( evt, data ) => {
			data.preventDefault();

			// The hard enter key is handled by the Enter plugin.
			if ( !data.isSoft ) {
				return;
			}

			editor.execute( 'shiftEnter' );
			view.scrollToTheSelection();
		}, { priority: 'low' } );
	}
}

export default ShiftEnter;

import Plugin from '@ckeditor/ckeditor5-core/src/plugin.js';
import ShyCommand, {softHyphenCharacter} from '../command/shy.js';
import NbspCommand, {nbspCharacter} from '../command/nbsp.js';

export default class Hyphens extends Plugin {
    static get pluginName() {
        return 'Hyphens';
    }

    init() {
        this.editor.commands.add('insertShyEntity', new ShyCommand(this.editor));
        this.editor.commands.add('insertNbspEntity', new NbspCommand(this.editor));
        this.editor.keystrokes.set([ 'Ctrl', 'Shift', 173 ], 'insertShyEntity');
        this.editor.keystrokes.set([ 'Ctrl', 'space' ], 'insertNbspEntity');

        this.editor.conversion.for('editingDowncast').add(dispatcher => {
            dispatcher.on('insert:$text', (event, data, conversionApi) => {
                // Here should be an `if` that would check whether the feature's command is enabled.
                if (!conversionApi.consumable.consume(data.item, 'insert')) {
                    return;
                }

                const viewWriter = conversionApi.writer;
                let modelPosition = data.range.start;
                let viewPosition = conversionApi.mapper.toViewPosition(modelPosition);
                
                const dataChunks = data.item.data.split(/([\u00AD,\u00A0])/);
                
                for (let i = 0; i < dataChunks.length; i++) {
                    const chunk = dataChunks[i];
                    
                    if (chunk === '') continue;

                    // Wrap special characters with spans and matching classes for styling
                    if (chunk === nbspCharacter || chunk === softHyphenCharacter) {
                        const characterClass = chunk === nbspCharacter ? 'nbsp' : 'shy';
                        
                        const viewSpaceSpan = viewWriter.createAttributeElement('span', {
                            class: characterClass,
                        }, {priority:100});

                        viewSpaceSpan._appendChild(viewWriter.createText(chunk));
                        viewWriter._insertNodes(viewPosition, [viewSpaceSpan], false);
                    }
                    else {
                        viewWriter.insert(viewPosition, viewWriter.createText(chunk));
                    }

                    // Need to recalculate `viewPosition` after every inserted item.
                    modelPosition = modelPosition.getShiftedBy(chunk.length);
                    viewPosition = conversionApi.mapper.toViewPosition(modelPosition);
                }
            }, {priority: 'low'});
        });
    }
}
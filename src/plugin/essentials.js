import Essentials from '@ckeditor/ckeditor5-essentials/src/essentials.js';
import ShiftEnter from './shiftenter.js';

/**
 * @license Copyright (c) 2003-2021, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or https://ckeditor.com/legal/ckeditor-oss-license
 */

/**
 * @inheritDoc
 *
 * Additionally, it replaces module:enter/shiftenter~ShiftEnter with ShiftEnter
 *
 * @extends {Essentials}
 */
export default class FumanEssentials extends Essentials {
	/**
	 * @inheritDoc
	 */
	static get requires() {
		/** @type {Plugin[]} requires */
		const requires = Essentials.requires.filter(plugin=>plugin.pluginName !== ShiftEnter.pluginName);
		requires.push(ShiftEnter);
		return requires;
	}

	/**
	 * @inheritDoc
	 */
	static get pluginName() {
		return 'Essentials';
	}
}
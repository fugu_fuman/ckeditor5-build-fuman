import Plugin from '@ckeditor/ckeditor5-core/src/plugin.js';
import LinkUI from '@ckeditor/ckeditor5-link/src/linkui.js';
import LinkEditing from '@ckeditor/ckeditor5-link/src/linkediting.js';
import FumanAutoLink from './autolink.js';

export default class Link extends Plugin {
    static get pluginName(){
        return 'Link';
    }

    /**
     * @inheritDoc
     */
    static get requires() {
        return [ LinkEditing, LinkUI, FumanAutoLink];
    }
}
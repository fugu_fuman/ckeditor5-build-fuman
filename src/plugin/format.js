import Block from './block.js';
import {FormatCommandView} from '../view.js';

export default class Format extends Block {
    static get pluginName(){
        return 'format';
    }

    init(){
        const options = this.editor.config.get( 'fuman' );
        this.defaultTitle = options.character_label
        this.items = options.characters;
        this.editor.ui.componentFactory.add( Format.pluginName, this.buildComponent);
    }
    
    getCommand(descriptor) {
        return this.editor.commands.get( descriptor.modelName);
    }

    createCommandView(){
        return new FormatCommandView(this.editor, this.dropdownView);
    }

    updateDropdown = (event, property, value, state) => {
        const buttonView = this.dropdownView.buttonView;
        // {event.source.value} is true when attribute is detected, false when removed/not detected
        if(event.source.value){
            buttonView.label = this.titles[event.source.attributeKey]
        }
        else if(buttonView.label === this.titles[event.source.attributeKey]){
            buttonView.label = this.defaultTitle;
        }
    }
}
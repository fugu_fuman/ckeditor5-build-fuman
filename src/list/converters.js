import {generateLiInUl, injectViewList} from '@ckeditor/ckeditor5-list/src/utils.js';

const attributes = [
    'listType', 'listIndent', 'classes'
]

/**
 * A model-to-view converter for the `listItem` model element insertion.
 *
 * It creates a `<ul><li></li><ul>` (or `<ol>`) view structure out of a `listItem` model element, inserts it at the correct
 * position, and merges the list with surrounding lists (if available).
 *
 * @see module:engine/conversion/downcastdispatcher~DowncastDispatcher#event:insert
 * @param {module:engine/model/model~Model} model Model instance.
 * @returns {Function} Returns a conversion callback.
 */
export function modelViewInsertion( model ) {
    return ( evt, data, conversionApi ) => {
        const consumable = conversionApi.consumable;

        if ( !consumable.test( data.item, 'insert' ) ||
            !attributes.every(name=>consumable.test(data.item, 'attribute:' + name))
        ) {
            return;
        }

        consumable.consume( data.item, 'insert' );
        attributes.forEach(name=>consumable.consume(data.item, 'attribute:' + name));

        const modelItem = data.item;
        const viewItem = generateLiInUl( modelItem, conversionApi );

        injectViewList( modelItem, viewItem, conversionApi, model );
    };
}


/**
 * A view-to-model converter that converts the `<li>` view elements into the `listItem` model elements.
 *
 * To set correct values of the `listType` and `listIndent` attributes the converter:
 * * checks `<li>`'s parent,
 * * stores and increases the `conversionApi.store.indent` value when `<li>`'s sub-items are converted.
 *
 * @see module:engine/conversion/upcastdispatcher~UpcastDispatcher#event:element
 * @param {module:utils/eventinfo~EventInfo} evt An object containing information about the fired event.
 * @param {Object} data An object containing conversion input and a placeholder for conversion output and possibly other values.
 * @param {module:engine/conversion/upcastdispatcher~UpcastConversionApi} conversionApi Conversion interface to be used by the callback.
 */
export function viewModelConverter( evt, data, conversionApi ) {
    if ( conversionApi.consumable.consume( data.viewItem, { name: true } ) ) {
        const writer = conversionApi.writer;

        // 1. Create `listItem` model element.
        const listItem = writer.createElement( 'listItem' );

        // 2. Handle `listItem` model element attributes.
        const indent = getIndent( data.viewItem );

        writer.setAttribute( 'listIndent', indent, listItem );

        // Set 'bulleted' as default. If this item is pasted into a context,
        const type = data.viewItem.parent && data.viewItem.parent.name === 'ol' ? 'numbered' : 'bulleted';
        writer.setAttribute( 'listType', type, listItem );
        
        
        writer.setAttribute( 'classes', data.viewItem.parent.getAttribute('class'));

        if ( !conversionApi.safeInsert( listItem, data.modelCursor ) ) {
            return;
        }

        const nextPosition = viewToModelListItemChildrenConverter( listItem, data.viewItem.getChildren(), conversionApi );

        // Result range starts before the first item and ends after the last.
        data.modelRange = writer.createRange( data.modelCursor, nextPosition );

        conversionApi.updateConversionResult( listItem, data );
    }
}
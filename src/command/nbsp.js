import Command from '@ckeditor/ckeditor5-core/src/command.js';

export const nbspCharacter = '\u00A0';

export default class NbspCommand extends Command {
    execute() {
        this.editor.model.change((writer) => {
            writer.insertText(nbspCharacter, this.editor.model.document.selection.getFirstPosition());
        });
    }
}
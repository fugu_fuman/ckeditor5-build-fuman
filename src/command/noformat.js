import Command from '@ckeditor/ckeditor5-core/src/command.js';

export class NoFormatCommand extends Command {
    /** @type string[] */
    attributes;
    
    constructor(editor) {
        super(editor);
        this.attributes = [];
    }

    /**
     * Executes the command &mdash; removes all fuman attributes from the selection.
     *
     * @fires execute
     * otherwise the command will remove the attribute.
     * If not set, the command will look for its current value to decide what it should do.
     */
    execute() {
        const model = this.editor.model;
        const doc = model.document;
        const selection = doc.selection;

        model.change( writer => {
            if ( selection.isCollapsed ) {
                writer.removeSelectionAttribute( this.attributes );
            }
            else {
                const ranges = new Set(this.attributes.reduce((attrs, attributeKey)=>attrs.concat([...model.schema.getValidRanges( selection.getRanges(), attributeKey )]), []));

                for (const curRange of ranges) {
                    for (const item of curRange.getItems()) {
                        // Workaround for items with multiple removable attributes. See
                        // https://github.com/ckeditor/ckeditor5-remove-format/pull/1#pullrequestreview-220515609
                        const itemRange = writer.createRangeOn(item);
                        for (const attributeName of item.getAttributeKeys()) {
                            writer.removeAttribute(attributeName, itemRange);
                        }
                    }
                }
            }
        } );
    }
}
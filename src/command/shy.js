import Command from '@ckeditor/ckeditor5-core/src/command.js';

export const softHyphenCharacter = '\u00AD';

export default class ShyCommand extends Command {
    execute() {
        this.editor.model.change((writer) => {
            writer.insertText(softHyphenCharacter, this.editor.model.document.selection.getFirstPosition());
        });
    }
}
import AttributeCommand from '@ckeditor/ckeditor5-basic-styles/src/attributecommand.js';

/**
 * @license Copyright (c) 2003-2021, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or https://ckeditor.com/legal/ckeditor-oss-license
 */

/**
 * An extension of the base {@link module:core/command~Command} class, which provides utilities for a command
 * that toggles a single attribute on a text or an element.
 *
 * `AttributeCommand` uses {@link module:engine/model/document~Document#selection}
 * to decide which nodes (if any) should be changed, and applies or removes the attribute from them.
 *
 * The command checks the {@link module:engine/model/model~Model#schema} to decide if it can be enabled
 * for the current selection and to which nodes the attribute can be applied.
 *
 * @extends module:core/command~Command
 */
export default class FormatCommand extends AttributeCommand {
    /**
     * Executes the command &mdash; applies the attribute to the selection or removes it from the selection.
     *
     * If the command is active (`value == true`), it will not remove attributes and does nothing. Otherwise, it will set attributes.
     *
     * The execution result differs, depending on the {@link module:engine/model/document~Document#selection}:
     *
     * * If the selection is on a range, the command applies the attribute to all nodes in that range
     * (if they are allowed to have this attribute by the {@link module:engine/model/schema~Schema schema}).
     * * If the selection is collapsed in a non-empty node, the command applies the attribute to the
     * {@link module:engine/model/document~Document#selection} itself (note that typed characters copy attributes from the selection).
     * * If the selection is collapsed in an empty node, the command applies the attribute to the parent node of the selection (note
     * that the selection inherits all attributes from a node if it is in an empty node).
     *
     * @fires execute
     * @param {Object} [options] Command options.
     * @param {Boolean} [options.forceValue] If set, it will force the command behavior. If `true`, the command will apply the attribute,
     * otherwise the command will remove the attribute.
     * If not set, the command will look for its current value to decide what it should do.
     */
    execute( options = {} ) {
        const model = this.editor.model;
        const doc = model.document;
        const selection = doc.selection;
        const value = ( options.forceValue === undefined ) ? !this.value : options.forceValue;

        model.change( writer => {
            if ( selection.isCollapsed ) {
                if ( value ) {
                    writer.setSelectionAttribute( this.attributeKey, true );
                }
            } else {
                const ranges = model.schema.getValidRanges( selection.getRanges(), this.attributeKey );

                for ( const range of ranges ) {
                    if ( value ) {
                        writer.setAttribute( this.attributeKey, value, range );
                    }
                }
            }
        });
    }
}


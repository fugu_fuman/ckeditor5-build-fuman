/**
 * @license Copyright (c) 2003-2021, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or https://ckeditor.com/legal/ckeditor-oss-license
 */

'use strict';

/* eslint-env node */
const path = require( 'path' );
const fs = require( 'fs' );
const webpack = require( 'webpack' );
const { bundler, styles } = require( '@ckeditor/ckeditor5-dev-utils' );
const CKEditorWebpackPlugin = require( '@ckeditor/ckeditor5-dev-webpack-plugin' );
const TerserPlugin = require( 'terser-webpack-plugin' );
const MiniCssExtractPlugin = require('mini-css-extract-plugin');


const postCssConfig = styles.getPostCssConfig( {
	themeImporter: {
		themePath: require.resolve( '@ckeditor/ckeditor5-theme-lark' )
	},
	minify: false
} );
postCssConfig.postcssOptions = {plugins: postCssConfig.plugins};
delete postCssConfig.plugins;

const entry = {};
const dist = path.resolve( __dirname, 'dist/@ckeditor');
fs.readdirSync(dist).forEach(function (file, index) {
	const sourcePath = path.join(dist, file, 'src', 'index.js');

	if(fs.existsSync(sourcePath)){
		entry[file] = sourcePath;
	}
	else {
		console.log('file not found', sourcePath);
	}
});

module.exports = {
	devtool: false,
	performance: { hints: false },
	experiments: {
		outputModule: true
	},

	entry: entry,

	output: {
		// The name under which the editor will be exported.
		// library: 'ClassicEditor',

		path: path.resolve( __dirname, 'build2' ),
		filename: '[name].js',
		libraryTarget: 'module',
		libraryExport: 'default'
	},

	optimization: {
		minimizer: [
			new TerserPlugin( {
				// sourceMap: true,
				terserOptions: {
					output: {
						// Preserve CKEditor 5 license comments.
						comments: /^!/
					}
				},
				extractComments: false
			} )
		]
	},

	plugins: [
		new CKEditorWebpackPlugin( {
			// UI language. Language codes follow the https://en.wikipedia.org/wiki/ISO_639-1 format.
			// When changing the built-in language, remember to also change it in the editor's configuration (src/ckeditor.js).
			language: 'en',
			additionalLanguages: 'all'
		} ),
		new webpack.BannerPlugin( {
			banner: bundler.getLicenseBanner(),
			raw: true
		} ),
		new MiniCssExtractPlugin()
	],

	module: {
		rules: [
			{
				test: /\.svg$/,
				use: 'raw-loader'
			},
			{
				test: /\.css$/i,
				use: [MiniCssExtractPlugin.loader, 'css-loader'],
			}
			// {
			// 	test: /\.css$/,
			// 	exclude: /.*/,
			// 	use: [
			// 		{
			// 			loader: 'style-loader',
			// 			options: {
			// 				injectType: 'singletonStyleTag',
			// 				attributes: {
			// 					'data-cke': true
			// 				}
			// 			}
			// 		},
			// 		{
			// 			loader: 'postcss-loader',
			// 			options: postCssConfig
			// 		}
			// 	]
			// }
		]
	}
};

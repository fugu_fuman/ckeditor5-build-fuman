/**
 * Some testes with snowpack, check if it is able to deliver usable es6 code
 */
import svg from 'rollup-plugin-svg';
import css from 'rollup-plugin-css-only';

/** @type {import("snowpack").SnowpackUserConfig } */
export default {
	type: 'test',
	workspaceRoot: '/dist',

	mount: {
		"src":    "/dist",
		"public": {url: "/", static: true, resolve: false}
	},

	buildOptions: {
		out: 'build-sp',
		metaUrlPath: './',
		sourcemap: false
	},

	plugins: [
		'snowpack-svgr-plugin',
	],

	packageOptions: {
		rollup: {plugins: [svg(), css({output:'main.css'})]}
	}
}
